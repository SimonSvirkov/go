package helper_test

import (
	"github.com/magiconair/properties/assert"
	"gitlab.com/go/helper"
	"testing"
)

func TestHello(t *testing.T) {
	assert.Equal(t, helper.Hello(), "hello")
}
